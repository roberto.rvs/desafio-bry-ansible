# desafio-bry-ansible

## Descrição

Esse projeto foi criado com o objetivo de automatizar via Ansible as instalações e atualizações do [pip](https://pypi.org/project/pip/), [Docker](https://www.docker.com/), [Docker Compose](https://docs.docker.com/compose/) e [Rancher 1.6](https://rancher.com/docs/rancher/v1.6/en/) através das roles ``common`` e ``rancher``.

 Caso seja de seu interesse entender como foi realizado o provisionamento da máquina utilizada para o desafio via Terraform, favor dirija-se ao repositório [desafio-bry-terraform](https://gitlab.com/roberto.rvs/desafio-bry).

## Roles

### Common

Essa role é encarregada de realizar a atualização dos pacotes da vm, instalar novos pacotes utils para o CentOS, o pip, o docker-py que será utilizado na role ``Rancher``, o Docker e o Docker Compose.

Para acessar a pasta da role common clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-ansible/tree/master/ansible/roles/common).

### Rancher

Essa role é encarregada de realizar a instalação do Rancher Server 1.6 através do docker-py instalado anteriormente.

Para acessar a pasta da role rancher clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-ansible/tree/master/ansible/roles/rancher).

## Gitlab CI

Nesse CI optei por criar apenas um estágio ``deploy``:

```yml
image:
  name: williamyeh/ansible:ubuntu18.04

before_script:
  - apt-get update -qq
  - apt-get install -qq git
  # Setup SSH deploy keys
  - 'which ssh-agent || ( apt-get install -qq openssh-client )'
  - eval $(ssh-agent -s)
  - ssh-add <(echo "$SSH_PRIVATE_KEY")
  - mkdir -p ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  # Create inventory file
  - echo '[desafio_bry]' > inventory
  - echo $DNS_DESAFIO ansible_user=$OS_USER >> inventory

stages:
  - deploy

deploy:
  stage: deploy
  script:
    - ansible-playbook ansible/main.yml -i inventory -vv
  when: manual
```

Em ``before_scripts`` é realizada a instalação do openssh-client e adicionada a chave privada contida na variável protegida **$SSH_PRIVATE_KEY** ao container para ser executado posteriormente pelo Ansible.

Logo abaixo é criado um arquivo de inventário contendo a variável **$DNS_DESAFIO** que possui o DNS da máquina e o **$OS_USER** que contém o usuário referente à chave ssh.

Depois, é executado o comando para inicialização do Ansible:

```yml
ansible-playbook ansible/main.yml -i inventory -vv
```

Para acessar o arquivo compleo do CI clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-ansible/blob/master/.gitlab-ci.yml).

Para visualizar os logs do último job rodado clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-ansible/-/jobs/390113853).

Pronto!
